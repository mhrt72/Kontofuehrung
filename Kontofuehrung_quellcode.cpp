/* kontoführung.c : Definiert den Einstiegspunkt für die Konsolenanwendung.

Lösung:
es wurden zwei Dateien konzipiert
person.txt: beinhaltet die Personen und die Kontonummern der Personen
buchung.txt: beinhaltet die Buchungen aller Konten

Bei der Entwickelung der Personendatei fiel die unterscheidliche Behandlung 
der Buchstaben, insbesondere der Umlaute und des ß auf. 
Die Umlaute und das "ß" müssen durch deren pendants (ae, ue, oe, ss) ersetzt werden. 
*/

//**********************************************************************
// Includes
//**********************************************************************
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

//**********************************************************************
// Defines
//**********************************************************************
#define MAXKTO 1000													    //maximale Anzahl von Konten
#define PERSDAT "person.txt"											//Kontennummern von Personen
#define KTODAT "konto.txt"
#define ANZEIGE 0														//Anzeige aller Dateiereignisse 1,Produktion 0

//**********************************************************************
// Globale Variablen
//**********************************************************************
struct person {															//Personendateisatz
	char name[50];														//Name
	char vorname[50];													//Vorname
	int ktonr;															//KTONR
};
struct person plesen = { "", "",0 };									//Struktur zum Lesen
struct person pschreiben = { "", "",0 };								//Struktur zum Schreiben

struct konto {															//Kotendateisatz
	int ktonr;															//KTONR
	int buchtyp;														//Buchung 1=Einlage 2=Ausgabe
	char buchungstext[100];												//Text
	char bdatum[11];
	int wert;															//Wert
};
struct konto klesen = { 0, 0, "", "",0 };								//KTO lesen
struct konto kschreiben = { 0, 0, "","", 0 };							//KTO schreiben

int ktn[MAXKTO];														//Ktonrfeld

//**********************************************************************
// Funktionen
//**********************************************************************
int zeige_menue();														//Menue zeigen
int personkonto_zeigen();
int personkonto_anlegen();
int person_eingeben();
int person_lesen(FILE *per);											//Person-Satz lesen
int person_schreiben(FILE *per);										//Person-Satz schreiben
FILE *datei_oeffnen(char *name, char *facc);							//Öffnen einer datei mit Namen name und Access facc,gibt filedescr-Zeiger
int datei_schliessen(FILE *fp, char *name);								//Schliessen einer Datei name
int kto_lesen(FILE *kto);
int kto_schreiben(FILE *kto);
int konto_zeigen(int knr);
char *tagesdatum();
int kontobuchungen_lesen();
int kontobuchungen_anlegen();

//**********************************************************************
// Hauptprogramm
//**********************************************************************
int main() {
	int n = 0;
	int e = 0;
	printf("Initialisierung\n");
	for (n = 0; n < MAXKTO; n++) ktn[n] = 0;							//Kontonummernfeld reset
	FILE *per = datei_oeffnen(PERSDAT, "r");							//Datei öffnen
	while ((e = person_lesen(per)) == 0) ktn[plesen.ktonr] = plesen.ktonr;	//Datei durchsehen KTONR belegen
	if (e == 2) return 2;												//Lesefehler
	datei_schliessen(per, PERSDAT);										//Datei schliessen
	printf("\n\n");
	zeige_menue();														//Menü zeigen und auswählen
	return 0;
}

//**********************************************************************
// Funktion zeige_menue
//**********************************************************************
int zeige_menue() {
	int m = 0;															//Menuepunkt reset
	int e = 0;
	char c = 'a';
	fflush(stdin);
	while (m != 9) {													//solange menu nicht 9
		m = NULL;
		printf("Kontofuehrung\n\n");									//Überschrift
		printf("1. Person/Konto anlegen\n");							//1. Punkt
		printf("2. Person/Konto zeigen\n");								//2. Punkt  //Nur Kontostand abfragen
		printf("3. Buchungen eines Kontos zeigen\n");					//3. Punkt
		printf("4. Buchungen eines Kontos anlegen\n\n");				//4. Punkt
		printf("9. Ende Programm\n\n");									//9. Punkt
		scanf("%d%c", &m, &c);											//Eingabe
		while (c != '\n') c = getchar();
		fflush(stdin);
		switch (m) {													//switch Menüpunkte
		case 1:
			personkonto_anlegen();										//Gesamtbilder
			break;
		case 2:
			e = personkonto_zeigen();									//Personendatei zeigen
			if (e == 1) return 1;										//Fehler
			break;
		case 3:
			e =kontobuchungen_lesen();									//Personendatei zeigen
			break;
		case 4:
			e = kontobuchungen_anlegen();								//Personendatei zeigen
			break;
		}
		printf("\n\n");													//neue Anzeige
	}
	return 0;															//wenn Programmende
}

//**********************************************************************
// Funktion personkonto_zeigen
//**********************************************************************
int personkonto_zeigen() {
	int e = 0;
	person_eingeben();													//Dateneingabe
	FILE *per = datei_oeffnen(PERSDAT, "r");							//Datei öffnen
	while ((e = person_lesen(per)) == 0) {								//Datei durchsehen
		if ((strcmp(plesen.name, pschreiben.name) == 0) && (strcmp(plesen.vorname, pschreiben.vorname) == 0)) {
			printf("Name:\t\t%s\nVorname:\t%s\nKonto:\t%d\n", plesen.name, plesen.vorname, plesen.ktonr);
			konto_zeigen(plesen.ktonr);									//Konto auslesen
			break;
			}
	}
	datei_schliessen(per, PERSDAT);										//Datei schliessen
	return e;															//Tschüss n=0 ist success
}

//**********************************************************************
// Funktion personkonto_anlegen
//**********************************************************************
int personkonto_anlegen() {
	int n = 0;
	int e = personkonto_zeigen();
	if (e == 0) {
		printf("Ein Kunde darf nur ein Konto besitzen!");				//Meldung
		return 0;
	}
	FILE *per = datei_oeffnen(PERSDAT, "a");							//zum anhängen öffnen
	for (n = 1; n < MAXKTO; n++) if (ktn[n] == 0) break;				//Kontonummernfeld reset
	if (n == MAXKTO) return 2;											//maximale kontozahl erreicht
	ktn[n] = n;															//Konto gefunden in Liste
	pschreiben.ktonr = n;												//in struktur
	person_schreiben(per);												//Struktur in datei sichern
	fflush(per);														//Schreiben erzwingen
	datei_schliessen(per, PERSDAT);										//Datei schliessen
	FILE *kto = datei_oeffnen(KTODAT, "a");								//zum Anhängen öffnen
	kschreiben.ktonr = ktn[n];											//in Buchungsdatei
	strcpy(kschreiben.buchungstext, "Anfangsbuchung");					//Text einschreiben
	kschreiben.buchtyp = 1;												//typ einschreiben
	strcpy(kschreiben.bdatum, tagesdatum());							//Datum einschreiben
	kschreiben.wert = 0;												//Wert einschreiben
	kto_schreiben(kto);													//in Datei
	datei_schliessen(kto, KTODAT);										//Datei schliessen
	printf("Fuer den Kunden %s %s wurde die Kontonummer %d angelegt\n"
		, pschreiben.name, pschreiben.vorname, pschreiben.ktonr);		//in datei schreiben
	return 0;															//Tschüss n=0 ist success
}

//**********************************************************************
// Funktion person_eingeben
//**********************************************************************
int person_eingeben() {
	char c = 'a';
	printf("Name des Kontoinhabers:\t\t");								//name eingeben
	scanf("%[a-zA-Z-]%c", &pschreiben.name, &c);						//holen
	printf("Vorname des Kontoinhabers:\t");
	scanf("%[a-zA-Z-]%c", &pschreiben.vorname, &c);
	printf("\n");
	return 0;
}

//**********************************************************************
// Funktion person_lesen
//**********************************************************************
int person_lesen(FILE *per) {											//
	int e;																//Fehler
	char k = 'w';
	clearerr(per);														//clear error Datei
																		//e = fread(&plesen, sizeof(plesen), 1, per);						//Einlesen mit Struktur
	e = fscanf(per, "%s%s%d", plesen.name, &plesen.vorname, &plesen.ktonr);	//Satz lesen e=Error
	if (e == EOF) return 1;
	return 0;
}

//**********************************************************************
// Funktion person_schreiben
//**********************************************************************
int person_schreiben(FILE *per) {										//
	int e;																//Fehler
	clearerr(per);														//clear error Datei
																		//e = fwrite(&pschreiben, sizeof(pschreiben), 1, per);				//test scheitert an big/little-Endian
	e = fprintf(per, "%s\n%s\n%d\n", pschreiben.name, pschreiben.vorname, pschreiben.ktonr);	//Satz lesen e=Error
	if (e == 0) {														//Fehler
		if (ANZEIGE == 1)printf("Fehler gefunden\n");
		return 2;														//sonstiger Fehler
	}
	return 0;
}

//**********************************************************************
// Funktion datei_oeffnen
//**********************************************************************
FILE *datei_oeffnen(char *name, char *facc) {
	FILE *fd;															//Filedescr. Rückgabewert
	fd = fopen(name, facc);												//File öffnen
	if (fd == 0) {
		if (ANZEIGE == 1) printf("Datei '%s' kann nicht geoeffnet werden!\n", name);	//Fehler
		fd = fopen(name, "w");
		fclose(fd);
		datei_oeffnen(name, facc);
	}
	else {
		if (ANZEIGE == 1) printf("Datei '%s' wurde geoeffnet!\n\n", name);	//Erfolg
	}
	return fd;															//fd=0 ist Fehler
}

//**********************************************************************
// Funktion datei_schliessen
//**********************************************************************
int datei_schliessen(FILE *fp, char *name) {
	int n;																//Rückgabewert
	fflush(fp);															//html flushen, falls noch nicht geschrieben
	n = fclose(fp);														//html schliessen
	if (ANZEIGE == 1) {
		if (n == 0) printf("Datei '%s' wurde geschlossen!\n", name);	//erfolg
		else printf("Datei '%s' konnte nicht geschlossen werden!\n", name);	//Fehler
	}
	return n;															//Tschüss n=0 ist success
}

//**********************************************************************
// Funktion kto_lesen
//**********************************************************************
int kto_lesen(FILE *kto) {												//
	int *p = &plesen.ktonr;													//
	int e;																//Fehler
	clearerr(kto);														//clear error Datei
	e = fscanf(kto, "%d%s%d%s%d", &klesen.ktonr, &klesen.buchungstext,
		&klesen.buchtyp, &klesen.bdatum, &klesen.wert);					//Satz lesen e=Error
	if (e == EOF) return 1;
	return 0;
}

//**********************************************************************
// Funktion kto_schreiben
//**********************************************************************
int kto_schreiben(FILE *kto) {										//
	int e;																//Fehler
	clearerr(kto);														//clear error Datei
	e = fprintf(kto, "%d\n%s\n%d\n%s\n%d\n", kschreiben.ktonr, kschreiben.buchungstext,
		kschreiben.buchtyp, kschreiben.bdatum, kschreiben.wert);							//Satz lesen e=Error
	if (e == 0) {														//Fehler
		if (ANZEIGE == 1)printf("Fehler gefunden\n");
		return 2;														//sonstiger Fehler
	}
	return 0;
}

//**********************************************************************
// Funktion konto_zeigen
//**********************************************************************
int konto_zeigen(int knr) {
	FILE *kto = datei_oeffnen(KTODAT, "r");							//zum lesen öffnen
	int su = 0;														//Summand als Double
	int n = 0;															//Buchungszähler
	while (kto_lesen(kto) == 0) {										//alle sätze lesen
		if (klesen.ktonr == knr) {										//wenn ktonr
			n++;														//buchung
			if (klesen.buchtyp == 1) su += klesen.wert;					//auf summand addieren
			else su -= klesen.wert;										//sonst subtrhiereen
		}
	}
	datei_schliessen(kto, KTODAT);										//Datei schliessen
	printf("Das Konto %d hat %d Buchungen, Stand %d Euro.\n\n", knr, n, su);	//Anzeige
	return 0;															//Tschüss n=0 ist success
}

//**********************************************************************
// Funktion tagesdatum
//**********************************************************************
char *tagesdatum() {
	time_t tjetzt;
	struct tm *tmjetzt;
	char dat[11];
	time(&tjetzt);
	tmjetzt = localtime(&tjetzt);
	sprintf(dat, "%02d.%02d.%d", tmjetzt->tm_mday, tmjetzt->tm_mon + 1, tmjetzt->tm_year + 1900);
	return dat;
}

//**********************************************************************
// Funktion Kontobuchungen_lesen
//**********************************************************************
int kontobuchungen_lesen() {
	int e;
	int knr;
	char t1[30];
	int n = 0;
	printf("Kontonummer eingeben!\n");									//Abfrage Ktonr
	scanf ("%d", &knr);													//Eingabe
	FILE *kto = datei_oeffnen(KTODAT, "r");							//Datei öffnen
	printf("Kontonr. Datum       Buchung                       Typ  Wert(Euro)\n");
	while ((e = kto_lesen(kto)) == 0) {									//Datei durchsuchen
		if (klesen.ktonr == knr) {										//
			strncpy(t1, klesen.buchungstext, 30);						//Teilkopieren
			t1[29] = NULL;												//Abschluss
			printf("%-9d%-12s%-30s%-5d%d\n", 
			klesen.ktonr, klesen.bdatum, t1, klesen.buchtyp, klesen.wert); //Tabelle bauen
			n++;
		}
	}
	datei_schliessen(kto, KTODAT);										//Datei schliessen
	if (n == 0) printf("Konto nicht angelegt!\n");						//Konto nicht vorh.
}	

//**********************************************************************
// Funktion buchungen_anlegen
//**********************************************************************
int kontobuchungen_anlegen() {
	int e = 1;															//Errormerker													
	int flag = 0;														//Merker
	int flag1;															//Merker
	char c ='a';														//Buchunstext
	char txt[100];
	kontobuchungen_lesen();												//alle buchungen des Kontos
	printf("Eingabe Buchungstext:\t");									//Anzeige
	scanf("%s%c", &kschreiben.buchungstext, &c);						//Textabfrage
	fflush(stdin);														//Tastaurpuffer löschen
	while (flag == 0) {													//Eingabe Typ mit Fehlerkorr
		printf("Eingabe Buchungstyp (1 = Einzahlung, 2 = Auszahlung)\t");	//Abfrage
		scanf("%d%c", &kschreiben.buchtyp, &c);							//Eingabe
		while (c != '\n') c = getchar();								//löschen
		fflush(stdin);													//löschen
		int a[] = { 1,2 };												//zugelassene Werte
		int n;															//Zähler
		for (n = 0; n < sizeof(a) / 4; n++) {							//Integerwerte durchflöhen
			if (a[n] == kschreiben.buchtyp) {							//Test
				flag = 1;												//ok
				break;													//fertig
			}
		}
		if (flag == 0 ) printf("Eingabe nicht erlaubt, Neueingabe...\n");	//Meldung
	}
	char jn = 'n';														//JN-Abfrage vorbesetzung
	int buch = 0;
	while (jn != 'j'){													//buchungswert bestätigen
		printf("Eingabe Wert:\t");										//Abfrage
		scanf("%d%c", &buch, &c);										//Eingabe		
		while (c != '\n') c = getchar();								//Löschen Whitespace
		fflush(stdin);													//löschen Tastatur
		printf("Wert %d\n", buch);										//Buchung anzeigen
		printf("Stimmt das(j/n)?\t");									//Bestätigung
		scanf("%c%c", &jn, &c);											//Eingabe
		fflush(stdin);													//löschen Tastatur
	}
	kschreiben.wert = buch;												//Buchung
	kschreiben.ktonr = klesen.ktonr;									//Kontonr kopieren								
	strcpy(kschreiben.bdatum, tagesdatum());							//Datum kopieren
	FILE *kto = datei_oeffnen(KTODAT, "a");								//zum anhängen öffnen
	kto_schreiben(kto);													//Satz schreiben
	datei_schliessen(kto, KTODAT);										//Datei schliessen
	return 0;
}