Unser Projekt hei�t Kontofuehrung.
Das Programm beinhaltet verschiedene Funktionen, wie eine Einloggen-Funktion, bei der die Kartennummer abgefragt wird. 
Der Nutzer soll Geld abheben, anlegen und seinen Kontostand abfragen k�nnen. 
Die Kontodaten sollen jeweils in einer Textdatei gespeichert werden. 
Bei Beenden des Programmes wird dem Nutzer "Karte wird ausgeben" eingeblendet.

Die gr��te Schwierigkeit besteht wohl darin, die Aktionen des Kontos in einer Textdatei zwischenzuspeichern, ohne direkt einen Saldo auszurechnen. 
Unser L�sungsansatz ist, die verscheidenen Aktionen zu speichern und auf eine Variable zu addieren, bzw zu subtrahieren und dann in der Aktion " Kontostand abfragen" auszugeben. 